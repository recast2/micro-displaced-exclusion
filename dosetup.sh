export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.22.00-python3-x86_64-centos7-gcc8-opt"
cd histfitter
source setup.sh
cd src
make clean
make
cd ../..
