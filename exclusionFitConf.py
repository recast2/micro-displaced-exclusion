from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from copy import deepcopy
from optparse import OptionParser
import sys,os 
from ROOT import gROOT, TLegend, TLegendEntry, TCanvas, TColor
import ROOT, argparse

#======================================================
#
#               Parse arguments
#
#=====================================================
inputParser = OptionParser()
inputParser.add_option('', '--RunToys',  dest = 'RunToys', default = '', help='Run frequentist exclusion fit, specifying number of toys.')
inputParser.add_option('', '--inNums',  dest = 'inNums', default = '', help='File that contains signal, bkg and observed number of events.')
inputParser.add_option('', '--mass',  dest = 'mass', default = '', help='Mass of BSM particle.')
inputParser.add_option('', '--lifetime',  dest = 'lifetime', default = '', help='Lifetime of BSM particle.')
inputParser.add_option('', '--isMultibin',  dest = 'isMultibin', default = True, help='True for multibin fit, false for just SR H.')
inputParser.add_option('', '--region',  dest = 'region', default = 'R1', help='which set of regions are being used, R1, R2, R3.')
configArgs = []
userArgs = configMgr.userArg.split(' ')
for arg in userArgs:
    configArgs.append(arg)
    print(arg)

(options, args) = inputParser.parse_args(configArgs)

#-------------------------------
# Parameters for hypothesis test
#-------------------------------

configMgr.fixSigXSec=True  # also does the hypotest for the up and down variations, and appends up(down) to the output filename
configMgr.testStatType=3   #3=one-sided profile likelihood test statistics, default LHC
configMgr.nPoints=20       #number of values scanned of signal-strength for upper-limit determination of signal strength.
if options.RunToys:
    print('Will run exclusion fit hypothesis tests using {0} toys.'.format(options.RunToys))
    configMgr.calculatorType=0 #0=frequentist,2=asymptotic calculator (creates asimov data set for the background hypothesis)
    configMgr.nTOYs=int(options.RunToys)
else:
    print('Will run exclusion fit hypothesis tests using asimov data set')
    configMgr.calculatorType=2 #0=frequentist,2=asymptotic calculator (creates asimov data set for the background hypothesis)

#-------------------------------
# Scaling outputLumi / inputLumi
#-------------------------------

configMgr.inputLumi = 1.0  # MC events are already weighted to lumi
configMgr.outputLumi = 1.0
configMgr.setLumiUnits("fb-1")
lumiError = 0.017

#------------------------------------------------------
# Get input numbers and name output files
#------------------------------------------------------
inNumbers = options.inNums
inNumsSplit = inNumbers.split(',')
isMultibin = int(options.isMultibin)
regionSet=options.region

mass=str(options.mass)
lifetime=str(options.lifetime)
out_name=regionSet
sig_name='mass_'+mass+'_lifetime_'+lifetime+'_'+regionSet
if isMultibin:
    sig_name=sig_name + '_multibin'
    out_name=out_name + '_multibin'
else:
    sig_name=sig_name + '_binH'
    out_name=out_name + '_binH'
    
configMgr.analysisName = sig_name
configMgr.histCacheFile = "output/"+out_name+".root"
configMgr.outputFileName = "output/"+out_name+".root"

nbkg_H = float(inNumsSplit[0])
bkgUncert_H = float(inNumsSplit[1])
ndata_A = float(inNumsSplit[2])
ndata_B = float(inNumsSplit[3])
ndata_C = float(inNumsSplit[4])
ndata_E = float(inNumsSplit[5])
ndata_H = float(inNumsSplit[6])

nsig_A = float(inNumsSplit[7])
sig_AErr = float(inNumsSplit[8])
nsig_B = float(inNumsSplit[9])
sig_BErr = float(inNumsSplit[10])
nsig_C = float(inNumsSplit[11])
sig_CErr = float(inNumsSplit[12])
nsig_E = float(inNumsSplit[13])
sig_EErr = float(inNumsSplit[14])
nsig_H = float(inNumsSplit[15])
sig_HErr = float(inNumsSplit[16])
xs_up = float(inNumsSplit[17])
xs_down = float(inNumsSplit[18])
max_mu_Sig=5.0
    
#======================================================
#
#                  Exclusion fit
#
#======================================================
# Define top-level                                    
ana = configMgr.addFitConfig("SPlusB_%s"%sig_name)

configMgr.doExclusion=True    

# Define weights, all event counts are already weighted so weight is just 1 
configMgr.weights = "1."

# Define cuts, events have already been through the cut selections so just set to 1
if isMultibin:
    configMgr.cutsDict["A"] = "1."
    configMgr.cutsDict["B"] = "1."
    configMgr.cutsDict["C"] = "1."
    configMgr.cutsDict["E"] = "1."
configMgr.cutsDict["H"] = "1."

# Define samples
dataSample = Sample("Data",kBlack)
dataSample.setData()
if isMultibin:
    dataSample.buildHisto([ndata_A],"A","cuts",0.5)
    dataSample.buildHisto([ndata_B],"B","cuts",0.5)
    dataSample.buildHisto([ndata_C],"C","cuts",0.5)
    dataSample.buildHisto([ndata_E],"E","cuts",0.5)
dataSample.buildHisto([ndata_H],"H","cuts",0.5)
        
sigSample = Sample(sig_name,kPink)
sigSample.setNormFactor("mu_Sig",1.,0.,max_mu_Sig)
if isMultibin:
    sigSample.buildHisto([nsig_A],"A","cuts",0.5)
    sigSample.buildHisto([nsig_B],"B","cuts",0.5)
    sigSample.buildHisto([nsig_C],"C","cuts",0.5)
    sigSample.buildHisto([nsig_E],"E","cuts",0.5)
sigSample.buildHisto([nsig_H],"H","cuts",0.5)
sigSample.setStatConfig(True) #This sample gets statistical uncertainties
if isMultibin:
    sigSample.buildStatErrors([sig_AErr],"A","cuts")
    sigSample.buildStatErrors([sig_BErr],"B","cuts")
    sigSample.buildStatErrors([sig_CErr],"C","cuts")
    sigSample.buildStatErrors([sig_EErr],"E","cuts")
sigSample.buildStatErrors([sig_HErr],"H","cuts")
sigSample.setNormByTheory() #uncertainties due to the luminosity are added

if isMultibin:
    val_bkg_A=ndata_A
    min_bkg_A=0.0001
    max_bkg_A=3000.0
    val_bkg_B=ndata_B
    min_bkg_B=0.0
    max_bkg_B=350.0
    val_bkg_C=ndata_C
    min_bkg_C=0.0
    max_bkg_C=350.0
    val_bkg_E=ndata_E/2
    min_bkg_E=0.00
    max_bkg_E=ndata_E
    val_bkg_H=ndata_H
    min_bkg_H=0.1
    max_bkg_H=20

    bkgSampleA = Sample("dummy_BkgA",kGreen-9)
    bkgSampleA.buildHisto([1.0],"A","cuts",0.5)
    bkgSampleA.buildHisto([0.0000001],"B","cuts",0.5)
    bkgSampleA.buildHisto([0.0000001],"C","cuts",0.5)
    bkgSampleA.buildHisto([0.0000001],"E","cuts",0.5)
    bkgSampleA.buildHisto([0.0000001],"H","cuts",0.5)
    bkgSampleA.addNormFactor("mu_A",val_bkg_A,min_bkg_A,max_bkg_A,False)

    bkgSampleB = Sample("dummy_BkgB",kGreen-9)
    bkgSampleB.buildHisto([0.0000001],"A","cuts",0.5)
    bkgSampleB.buildHisto([1.0],"B","cuts",0.5)
    bkgSampleB.buildHisto([0.0000001],"C","cuts",0.5)
    bkgSampleB.buildHisto([0.0000001],"E","cuts",0.5)
    bkgSampleB.buildHisto([0.0000001],"H","cuts",0.5)
    bkgSampleB.addNormFactor("mu_B",val_bkg_B,min_bkg_B,max_bkg_B,False)

    bkgSampleC = Sample("dummy_BkgC",kGreen-9)
    bkgSampleC.buildHisto([0.0000001],"A","cuts",0.5)
    bkgSampleC.buildHisto([0.0000001],"B","cuts",0.5)
    bkgSampleC.buildHisto([1.0],"C","cuts",0.5)
    bkgSampleC.buildHisto([0.0000001],"E","cuts",0.5)
    bkgSampleC.buildHisto([0.0000001],"H","cuts",0.5)
    bkgSampleC.addNormFactor("mu_C",val_bkg_C,min_bkg_C,max_bkg_C,False)

    bkgSampleE = Sample("dummy_BkgE",kGreen-9)
    bkgSampleE.buildHisto([0.0000001],"A","cuts",0.5)
    bkgSampleE.buildHisto([0.0000001],"B","cuts",0.5)
    bkgSampleE.buildHisto([0.0000001],"C","cuts",0.5)
    bkgSampleE.buildHisto([1.0],"E","cuts",0.5)
    bkgSampleE.buildHisto([0.0000001],"H","cuts",0.5)
    bkgSampleE.addNormFactor("mu_E",val_bkg_E,min_bkg_E,max_bkg_E,False)

    bkgSampleH = Sample("dummy_BkgH",kGreen-9)
    bkgSampleH.buildHisto([0.0000001],"A","cuts",0.5)
    bkgSampleH.buildHisto([0.0000001],"B","cuts",0.5)
    bkgSampleH.buildHisto([0.0000001],"C","cuts",0.5)
    bkgSampleH.buildHisto([0.0000001],"E","cuts",0.5)
    bkgSampleH.buildHisto([1.0],"H","cuts",0.5)
    bkgSampleH.addNormFactor("mu_H",val_bkg_H,min_bkg_H,max_bkg_H,False)

    ana.addFunction("mu_H","(mu_B*mu_C*mu_E)/(mu_A*mu_A)","mu_A[{0},{1},{2}],mu_B[{3},{4},{5}],mu_C[{6},{7},{8}],mu_E[{9},{10},{11}]".format(val_bkg_A,min_bkg_A,max_bkg_A,val_bkg_B,min_bkg_B,max_bkg_B,val_bkg_C,min_bkg_C,max_bkg_C,val_bkg_E,min_bkg_E,max_bkg_E) )
else:
    bkgSampleH = Sample("Bkg",kGreen-9)
    bkgSampleH.setStatConfig(True)
    bkgSampleH.buildHisto([nbkg_H],"H","cuts",0.5)
    bkgSampleH.buildStatErrors([bkgUncert_H],"H","cuts")
    
    
theoSysSig = Systematic("SigXSec", configMgr.weights, 1+xs_up, 1-xs_down, "user","userOverallSys")
sigSample.addSystematic(theoSysSig)

nonClosureSys = Systematic("nonClosure_syst", configMgr.weights, 1+0.4, 1-0.4, "user","userOverallSys")
bkgSampleH.addSystematic(nonClosureSys)

if isMultibin:
    ana.addSamples([bkgSampleA,bkgSampleB,bkgSampleC,bkgSampleE,bkgSampleH,sigSample,dataSample])
else:
    ana.addSamples([bkgSampleH,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define the measurement                              
meas = ana.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_Sig")
if isMultibin:
    meas.addParamSetting("mu_H", False, val_bkg_H)
    meas.addParamSetting("mu_A", False, val_bkg_A)
    meas.addParamSetting("mu_B", False, val_bkg_B)
    meas.addParamSetting("mu_C", False, val_bkg_C)
    meas.addParamSetting("mu_E", False, val_bkg_E)

# Add the channels
if isMultibin:
    chanA = ana.addChannel("cuts",["A"],1,0.5,1.5)
    chanB = ana.addChannel("cuts",["B"],1,0.5,1.5)
    chanC = ana.addChannel("cuts",["C"],1,0.5,1.5)
    chanE = ana.addChannel("cuts",["E"],1,0.5,1.5)
    ana.addBkgConstrainChannels([chanA,chanB,chanC,chanE])
chanH = ana.addChannel("cuts",["H"],1,0.5,1.5)
ana.addSignalChannels([chanH])
