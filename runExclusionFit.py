#!/usr/bin/env python
import os
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--lifetime",  help   = "lifetime of BSM particle", default="10")
parser.add_argument("--mass",  help   = "mass of BSM particle", default="300")
parser.add_argument("--region",  help   = "pick which set of regions you want", default="R1")
parser.add_argument("--isMultibin",  help   = "True(1) for multibin fit, false (0) for just SR H", default='1')

args = parser.parse_args()

M=args.mass
tau=args.lifetime
region_str=args.region
MB=str(args.isMultibin)
if(region_str=="R1"):
        region=2
elif(region_str=="R2"):
        region=3
else:
        region=4
counter=0
with open('yields.txt') as f:
        lines = f.read().splitlines()
        for line in lines:
                counter+=1
                #don't read in the description of the contents in the yields file
                if 'data' in line: continue
                if int(counter) == int(region):
                        os.system('HistFitter.py -u "--inNums ' + line +' --mass ' + M + ' --lifetime ' + tau + ' --isMultibin '+ MB + ' --region ' + region_str + '" -t -wp -f -D "corrMatrix" -F excl exclusionFitConf.py > output/logs/log.txt') 
                        break
