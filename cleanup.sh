#!/bin/bash
rm -rf data
rm -rf config
rm -rf interpolation 
rm -rf plots
rm -rf results
rm -rf output/*.root
rm -rf *.json
