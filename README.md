To run on lxplus do:
```
git clone --recursive https://gitlab.cern.ch/recast2/micro-displaced-exclusion.git
cd micro-displaced-exclusion
source dosetup.sh
python runExclusionFit.py --mass 300 --lifetime 10 --isMultibin 0 --region R1
python combineToJSON.py
```
NOTE clone with recursive option due to the sub module.

To use a Docker image do:
```
git clone --recursive https://gitlab.cern.ch/recast2/micro-displaced-exclusion.git
cd micro-displaced-exclusion
docker run --rm -it -v $PWD:/HF histfitter/histfitter:sha-e5980a0b bash
cd /HF
cd histfitter
source setup.sh
cd src
make clean
make
cd ../..
python runExclusionFit.py --mass 300 --lifetime 10 --isMultibin 0 --region R1
python combineToJSON.py
```