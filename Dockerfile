FROM histfitter/histfitter:sha-e5980a0b
ADD . /exclusion
WORKDIR /exclusion

USER root

SHELL ["/bin/bash", "-c"]

#add nobody to the root group

RUN usermod -aG root nobody && \
    chsh -s /bin/bash nobody && \ 
    su nobody && \ 
    id -Gn && \
    chown -R nobody /exclusion;